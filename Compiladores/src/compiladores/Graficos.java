/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author erick
 */
public class Graficos extends JPanel{
    
    ArrayList<Estados> afd;
    int larguraAcumulada = 0;
    
    public Graficos(ArrayList<Estados> afd){
        this.afd = afd;
    }
    
    
    
    @Override
    protected void paintComponent(Graphics g) { 
        
        //verifica quem é o nó inicial para iniciar a montagem da imagem
        for(int i=0; i<afd.size(); i++){
            if((afd.get(i).getTipo()!= null)&&(afd.get(i).getTipo().equals("inicial"))){
                montaImagem(g, afd.get(i), "centro", "", 0, 100);
            }
        }
        
    }
    
    public void montaImagem(Graphics g, Estados estado, String tipoSeta, String valor, int x, int y){
        
          
        if(tipoSeta.equals("centro")){
            setaReta(g, valor, x, y);
            no(g, estado, x+35, y);
        } 
        if(tipoSeta.equals("cima")){
            setaCima(g, valor, x, y);
            y-=40;
            no(g, estado, x+35, y);
        }
        if(tipoSeta.equals("baixo")){
            setaBaixo(g, valor, x, y);
            y+=40;
            no(g, estado, x+35, y);
        }
        
        for(int j=0; j<estado.getVaiParaA().size(); j++){
            montaImagem(g, buscaEstadoPeloNo(estado.getVaiParaA().get(j).getNome() , afd), "cima", "a", x+75, y);
        }
        for(int j=0; j<estado.getVaiParaB().size(); j++){
            montaImagem(g, buscaEstadoPeloNo(estado.getVaiParaB().get(j).getNome() , afd), "baixo", "b", x+75, y);
        }                      
        
        
    }
    
    public Graphics setaReta(Graphics g, String valor, int x, int y){
        //CONSTRUÇÃO DA SETA
        //seta reta tem largura de 35 altura de 0
        g.setColor( Color.BLACK );
        g.drawLine( x+30 , y+95, x+35, y+100 );
        g.drawLine( x+0, y+100, x+35 , y+100 );
        g.drawLine( x+30, y+105, x+35, y+100 );  
        g.setFont(new Font("Arial Bold", Font.PLAIN, 12));
        g.drawString(valor, x+15, y+99);
        return g;
    }
    
    public Graphics setaCima(Graphics g, String valor, int x, int y){
        //CONSTRUÇÃO DA SETA        
        //seta cima tem largura de 35 altura de 40
        g.setColor( Color.BLACK );
        g.drawLine( x+25, y+60, x+35, y+60 ); //ponta de cima
        g.drawLine( x+0, y+100, x+35, y+60 ); //reta
        g.drawLine( x+35, y+70, x+35, y+60 ); //ponta de baixo
        g.setFont(new Font("Arial Bold", Font.PLAIN, 12));
        g.drawString(valor, x+15, y+75);        
        
        return g;
    }
    public Graphics setaBaixo(Graphics g, String valor, int x, int y){
        //CONSTRUÇÃO DA SETA
        //seta baixo tem largura de 35 altura de 40
        g.setColor( Color.BLACK );
        g.drawLine( x+35, y+130, x+35, y+140 ); //ponta de cima
        g.drawLine( x+0, y+100, x+35, y+140 ); //reta
        g.drawLine( x+25, y+140, x+35, y+140 ); //ponta de baixo   
        g.setFont(new Font("Arial Bold", Font.PLAIN, 12));
        g.drawString(valor, x+15, y+135);
        return g;
    }
    
    
    public Graphics no(Graphics g, Estados estado, int x, int y){
        y+=80;
        if((estado.getTipo() != null)&&(estado.getTipo().equals("final"))){
            //INSERE BOLA PRETA PARA DESTACAR COMO NÓ FINAL
            g.setColor(Color.BLACK);
            g.fillOval(x+0, y+0, 40, 40);
        }
        //CONSTRUÇÃO DO NÓ
        g.setColor(Color.YELLOW);
        //fillOval(int x, int y, int width, int height)
        g.fillOval(x+5, y+5, 30, 30);
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial Bold", Font.PLAIN, 18));
        g.drawString(estado.getNome(), x+11, y+27);
        
        return g;
    }
    
    public Estados buscaEstadoPeloNo(String nomeNo, ArrayList<Estados> todosEstados){
        
        Estados estado = null;
        for(int i=0; i<todosEstados.size(); i++){
            if(todosEstados.get(i).getEpslonClausuraPara().equals(nomeNo)){
                 estado =  todosEstados.get(i);             
            }
        }
        
        return estado;
    }
    
}
