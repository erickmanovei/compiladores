/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

import java.util.ArrayList;

/**
 *
 * @author erick
 */
public class ConversaoAfd {
    
    ArrayList<Nos> afn;
    int contagemEstados =1;
    ArrayList<Estados> estados = new ArrayList<Estados>();

    public ConversaoAfd(ArrayList<Nos> afn) {
        this.afn = afn;
    }
    
    public ArrayList<Estados> realizaConversao(){
        //coleta o epsolon clausura do nó inicial
        for(int i=0; i<afn.size(); i++){
            if(afn.get(i).getTipo().equals("inicial")){
                Estados estadoQ1 = new Estados();
                estadoQ1.setNome("q1");
                estadoQ1.setEpslonClausuraPara(afn.get(0).getNome());
                estadoQ1.setEpslonClausura(epslonClausura(afn.get(0), null));
                estados.add(estadoQ1);
                montaEstados(estadoQ1);
            }                       
        }
        return this.estados;
    }
    
    
    //método recursivo que varre até onde vai com E
    public ArrayList<Nos> epslonClausura(Nos estado, ArrayList<Nos> eClausura){
       
       if(!jaExisteNo(estado.getNome(), eClausura)){
            if(eClausura == null){
                eClausura = new ArrayList<Nos>();
            } 

            eClausura.add(estado);
            if(!jaExisteEpslonClausuraPara(estado.getNome(), this.estados)){
                for(int i=0; i<estado.getArestas().size(); i++){
                    if(estado.getArestas().get(i).getValor().equals("E")){
                         epslonClausura(estado.getArestas().get(i).getNo(), eClausura);
                    }
                }  
            }
       }
       return eClausura;        
    }
    
    public void montaEstados(Estados estadoAnterior){
        
        //verifica se tem valores que vão para A para montar o novo estado
        if(estadoAnterior.getVaiParaA().size()>0){
            ArrayList<Nos> eClausura = new ArrayList<Nos>();
            for(int i=0; i<estadoAnterior.getVaiParaA().size(); i++){
                
                for(int e=0; e<epslonClausura(estadoAnterior.getVaiParaA().get(i) , null).size(); e++){
                    eClausura.add(epslonClausura(estadoAnterior.getVaiParaA().get(i) , null).get(e));
                } 
                
            }
            //verifica se já existe esse estado e só cria se ainda não existir
            if(!jaExisteEstado(eClausura.get(0).getNome(), estados)){
                contagemEstados++;
                Estados novoEstado = new Estados();
                novoEstado.setNome("q"+contagemEstados);
                novoEstado.setEpslonClausuraPara(eClausura.get(0).getNome());
                novoEstado.setEpslonClausura(eClausura);
                estados.add(novoEstado);
                montaEstados(novoEstado);
            }
        }
        
        //verifica se tem valores que vão para B para montar o novo estado
        if(estadoAnterior.getVaiParaB().size()>0){
            ArrayList<Nos> eClausura = new ArrayList<Nos>();
            for(int z=0; z<estadoAnterior.getVaiParaB().size(); z++){
                for(int x=0; x<epslonClausura(estadoAnterior.getVaiParaB().get(z) , null).size(); x++){
                    eClausura.add(epslonClausura(estadoAnterior.getVaiParaB().get(z) , null).get(x));
                }                
            }
            //verifica se já existe esse estado e só cria se ainda não existir
            if(!jaExisteEstado(eClausura.get(0).getNome(), estados)){
                contagemEstados++;
                Estados novoEstado = new Estados();
                novoEstado.setNome("q"+contagemEstados);
                novoEstado.setEpslonClausuraPara(eClausura.get(0).getNome());
                novoEstado.setEpslonClausura(eClausura);
                estados.add(novoEstado);
                montaEstados(novoEstado);
            }
        }        
    }   
    
    public String buscaEstadoPeloNo(String nomeNo, ArrayList<Estados> todosEstados){
        
        String nomeEstado = null;
        for(int i=0; i<todosEstados.size(); i++){
            if(todosEstados.get(i).getEpslonClausuraPara().equals(nomeNo)){
                 nomeEstado =  todosEstados.get(i).getNome();             
            }
        }
        
        return nomeEstado;
    }
    
    
    
    public boolean jaExisteEstado(String epslonClausuraPara, ArrayList<Estados> todosEstados){
        boolean resultado = false;
        
        for(int i=0; i<todosEstados.size(); i++){
            if(todosEstados.get(i).getEpslonClausuraPara().equals(epslonClausuraPara)){
                resultado = true;
            }
        }
        return resultado;        
    }
    
    public boolean jaExisteNo(String no, ArrayList<Nos> nos){
        boolean resultado = false;
        
        if(nos != null){
            for(int i=0; i<nos.size(); i++){
                if(nos.get(i).getNome().equals(no)){
                    resultado = true;
                }
            }
        }
        return resultado;        
    }
    
    public boolean jaExisteEpslonClausuraPara(String no, ArrayList<Estados> estados){
        boolean resultado = false;
        
        if(estados != null){
            for(int i=0; i<estados.size(); i++){
                if(estados.get(i).getEpslonClausuraPara().equals(no)){
                    resultado = true;
                }
            }
        }
        return resultado;        
    }
    
    
}