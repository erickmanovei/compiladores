/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

/**
 *
 * @author erick
 */
public class Arestas {
    private Nos no;
    private String valor;

    public Nos getNo() {
        return no;
    }

    public void setNo(Nos no) {
        this.no = no;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
