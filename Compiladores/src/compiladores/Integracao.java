/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author erick
 */
public class Integracao {
    
    public String[][] montarArrayString(String caminhoArquivo) throws FileNotFoundException, IOException{
        File file = new File(caminhoArquivo);
        int linhas;
        
        // pega o tamanho
        long tamanhoArquivo = file.length();
        FileInputStream fs = new FileInputStream(file);
        DataInputStream in = new DataInputStream(fs);
        
        LineNumberReader lineRead = new LineNumberReader(new InputStreamReader(in));
        lineRead.skip(tamanhoArquivo);      
        linhas = lineRead.getLineNumber() + 1;
        
                
        
        String[][] nos = new String[linhas][3];
        int count = 0;
        
        try {
            Scanner arq = new Scanner(file);
            
            while (arq.hasNextLine()) {
                String linha = arq.nextLine();
                //System.out.println(linha);
                
                String no[] = linha.split(Pattern.quote(","));
                nos[count][0] = no[0];
                nos[count][1] = no[1];
                if(count == 0){
                    nos[count][2] = "";
                }else{
                    nos[count][2] = no[2];
                }
                count++;
            }
            arq.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        return nos;
    }
    
    public ArrayList<Nos> montaListaNos(String caminhoArquivo) throws IOException{
        //monta o array de strings a partir do txt
        String[][] nos = montarArrayString(caminhoArquivo);        
        //cadastra os nos a partir do array de strings
        ArrayList<Nos> nosArray = cadastraNos(nos);
        //cadastra as arestas dos nós
        montarArestas(nos, nosArray);  
        //cadastra os tipos
        cadastraTipo(nos, nosArray);
        
        return nosArray;
    }
    
    public ArrayList<Nos> cadastraNos(String[][] nos){
        ArrayList<Nos> nosArray = new ArrayList<Nos>();
        
        for(int i=1; i<nos.length; i++){
            if(!verificaExistenciaDeNo(nos[i][0], nosArray)){
                Nos novoNo = new Nos();
                novoNo.setNome(nos[i][0]);
                nosArray.add(novoNo);
            }
            if(!verificaExistenciaDeNo(nos[i][2], nosArray)){
                Nos novoNo = new Nos();
                novoNo.setNome(nos[i][2]);
                nosArray.add(novoNo);
            }
        }
        return nosArray;
    }
    
    public boolean verificaExistenciaDeNo(String no, ArrayList<Nos> nos){
        boolean retorno = false;
        for(int i=0; i<nos.size(); i++){            
           if(nos.get(i).getNome().equals(no)){
               retorno = true;
           }     
        }
        return retorno;
    }
    
    public void montarArestas(String[][] nos, ArrayList<Nos> nosArray){
        System.out.println("tamanho " +nos.length);
        for(int i=0; i<nos.length -1; i++){
            System.out.println("No "+nos[i+1][0]);
            Nos no = retornaNo(nos[i+1][0], nosArray);
            Arestas aresta = new Arestas();
            aresta.setValor(nos[i+1][1]);
            aresta.setNo(retornaNo(nos[i+1][2], nosArray));
            no.addAresta(aresta);
        }
    }
    
    public Nos retornaNo(String nome, ArrayList<Nos> nosArray){
        Nos no = null;
        for(int i=0; i<nosArray.size(); i++){            
           if(nosArray.get(i).getNome().equals(nome)){
               no = nosArray.get(i);
           }     
        }
        return no;
    }
    
    public void cadastraTipo(String[][] nos, ArrayList<Nos> nosArray){
        
        
        for(int i=0; i<nosArray.size(); i++){
            if(nosArray.get(i).getNome().equals(nos[0][0])){
                nosArray.get(i).setTipo("inicial");
            }
            else if(nosArray.get(i).getNome().equals(nos[0][1])){
                nosArray.get(i).setTipo("final");
            }
            else{
                nosArray.get(i).setTipo("nenhum");
            }
            
        }
    }

    
}
