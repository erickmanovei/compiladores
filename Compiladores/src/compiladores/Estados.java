/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

import java.util.ArrayList;

/**
 *
 * @author erick
 */
public class Estados {
    
    private String nome;
    private String tipo;
    private String epslonClausuraPara;
    private ArrayList<Nos> epslonClausura;
    private ArrayList<Nos> vaiParaA = new ArrayList<Nos>();
    private ArrayList<Nos> vaiParaB = new ArrayList<Nos>();
    
    public String getEpslonClausuraPara() {
        return epslonClausuraPara;
    }

    public void setEpslonClausuraPara(String epslonClausuraPara) {
        this.epslonClausuraPara = epslonClausuraPara;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ArrayList<Nos> getVaiParaA() {
        return vaiParaA;
    }

    public void setVaiParaA(ArrayList<Nos> vaiParaA) {
        this.vaiParaA = vaiParaA;
    }

    public ArrayList<Nos> getVaiParaB() {
        return vaiParaB;
    }

    public void setVaiParaB(ArrayList<Nos> vaiParaB) {
        this.vaiParaB = vaiParaB;
    }
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Nos> getEpslonClausura() {
        return epslonClausura;
    }

    public void setEpslonClausura(ArrayList<Nos> epslonClausura) {
        
        //Antes de setar a epslon clausura Verifica quem vai para A e quem vai para B e atribui à sua respectiva arraylist
        for(int i=0; i<epslonClausura.size(); i++){
            //percorre as arestas para verificar seus valores
            for(int e=0; e<epslonClausura.get(i).getArestas().size(); e++){
                if(epslonClausura.get(i).getArestas().get(e).getValor().equals("a")){
                    this.vaiParaA.add(epslonClausura.get(i).getArestas().get(e).getNo());
                }else if(epslonClausura.get(i).getArestas().get(e).getValor().equals("b")){
                    this.vaiParaB.add(epslonClausura.get(i).getArestas().get(e).getNo());
                }            
            }
            
            //Ao percorrer o arraylist, verifica se existe algum nó inicial ou final, para definir o tipo de estado
            if(epslonClausura.get(i).getTipo().equals("inicial")){
                this.tipo = "inicial";
            }
            if(epslonClausura.get(i).getTipo().equals("final")){
                this.tipo = "final";
            }
        }      
        this.epslonClausura = epslonClausura;
    }   
}
