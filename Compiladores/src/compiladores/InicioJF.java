/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

import java.awt.Component;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author erick
 */
public class InicioJF extends javax.swing.JFrame {
    
    InicioJF inicio = this;
    private ArrayList<Nos> afn = new ArrayList<Nos>();   

    public ArrayList<Nos> getAfn() {
        return afn;
    }

    public void setAfn(ArrayList<Nos> afn) {
        this.afn = afn;
    }
    
    public void habilitarAfd(Boolean a){
        menu_afd.setEnabled(a);
    }

    /**
     * Creates new form InicioJF
     */
    public InicioJF() {
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escolherArquivo = new javax.swing.JFileChooser();
        jdpPrincipal = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        menu_conversao = new javax.swing.JMenu();
        menu_afn = new javax.swing.JMenu();
        informarAfnManualmente = new javax.swing.JMenuItem();
        informarAfnImportar = new javax.swing.JMenuItem();
        menu_afd = new javax.swing.JMenuItem();
        menu_ajuda = new javax.swing.JMenu();
        menu_manual = new javax.swing.JMenuItem();
        menu_sobre = new javax.swing.JMenuItem();
        menu_sair = new javax.swing.JMenu();

        escolherArquivo.setDialogTitle("Escolha um arquivo txt");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Conversão AFN para AFD");
        getContentPane().setLayout(null);

        javax.swing.GroupLayout jdpPrincipalLayout = new javax.swing.GroupLayout(jdpPrincipal);
        jdpPrincipal.setLayout(jdpPrincipalLayout);
        jdpPrincipalLayout.setHorizontalGroup(
            jdpPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
        );
        jdpPrincipalLayout.setVerticalGroup(
            jdpPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 460, Short.MAX_VALUE)
        );

        getContentPane().add(jdpPrincipal);
        jdpPrincipal.setBounds(10, 10, 720, 460);

        menu_conversao.setMnemonic('C');
        menu_conversao.setText("Conversão");
        menu_conversao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_conversaoMouseClicked(evt);
            }
        });
        menu_conversao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_conversaoActionPerformed(evt);
            }
        });

        menu_afn.setText("Informar AFN");
        menu_afn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        menu_afn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_afnMouseClicked(evt);
            }
        });
        menu_afn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_afnActionPerformed(evt);
            }
        });

        informarAfnManualmente.setText("Manualmente");
        informarAfnManualmente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informarAfnManualmenteActionPerformed(evt);
            }
        });
        menu_afn.add(informarAfnManualmente);

        informarAfnImportar.setText("Importar");
        informarAfnImportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                informarAfnImportarActionPerformed(evt);
            }
        });
        menu_afn.add(informarAfnImportar);

        menu_conversao.add(menu_afn);

        menu_afd.setText("Converter AFD");
        menu_afd.setEnabled(false);
        menu_afd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_afdActionPerformed(evt);
            }
        });
        menu_conversao.add(menu_afd);

        jMenuBar1.add(menu_conversao);

        menu_ajuda.setMnemonic('A');
        menu_ajuda.setText("Ajuda");

        menu_manual.setText("Manual do Sistema");
        menu_manual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_manualActionPerformed(evt);
            }
        });
        menu_ajuda.add(menu_manual);

        menu_sobre.setText("Sobre");
        menu_sobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_sobreActionPerformed(evt);
            }
        });
        menu_ajuda.add(menu_sobre);

        jMenuBar1.add(menu_ajuda);

        menu_sair.setMnemonic('s');
        menu_sair.setText("Sair");
        menu_sair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_sairMouseClicked(evt);
            }
        });
        menu_sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_sairActionPerformed(evt);
            }
        });
        jMenuBar1.add(menu_sair);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(757, 543));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menu_sairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_sairMouseClicked
        // TODO add your handling code here:
        System.exit(0);
        
    }//GEN-LAST:event_menu_sairMouseClicked

    private void menu_conversaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_conversaoActionPerformed
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_menu_conversaoActionPerformed

    private void menu_conversaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_conversaoMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_menu_conversaoMouseClicked

    private void menu_sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_sairActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_menu_sairActionPerformed

    private void menu_afnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_afnActionPerformed
        // TODO add your handling code here: 
    }//GEN-LAST:event_menu_afnActionPerformed

    private void menu_afnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_afnMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_menu_afnMouseClicked

    private void informarAfnManualmenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informarAfnManualmenteActionPerformed
        // TODO add your handling code here:
        AfnJIF afnjif = new AfnJIF(inicio);
        jdpPrincipal.add(afnjif);
        afnjif.setVisible(true);
    }//GEN-LAST:event_informarAfnManualmenteActionPerformed

    private void menu_afdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_afdActionPerformed
        // TODO add your handling code here:
        if(afn.size() < 1){
            JOptionPane.showMessageDialog(null, "É necessário informar o AFN na opção Informar AFN para acessar essa opção.");
        }else{
            AfdJIF afdjif = new AfdJIF(afn);
            jdpPrincipal.add(afdjif);
            afdjif.setVisible(true);
        }
    }//GEN-LAST:event_menu_afdActionPerformed

    private void menu_manualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_manualActionPerformed
        try {
            // TODO add your handling code here:
            java.awt.Desktop.getDesktop().browse( new java.net.URI( "http://compiladoresdp2.wix.com/manual1" ));
        } catch (URISyntaxException ex) {
            Logger.getLogger(InicioJF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InicioJF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menu_manualActionPerformed

    private void menu_sobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_sobreActionPerformed
        // TODO add your handling code here:
        SobreJIF sobre = new SobreJIF();
        jdpPrincipal.add(sobre);
        sobre.setVisible(true);
    }//GEN-LAST:event_menu_sobreActionPerformed

    private void informarAfnImportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_informarAfnImportarActionPerformed
        // TODO add your handling code here:
        int returnVal = escolherArquivo.showOpenDialog(this);
        if (returnVal == escolherArquivo.APPROVE_OPTION) {
            
            File file = escolherArquivo.getSelectedFile();
            System.out.println(file.getAbsolutePath());
            Integracao integracao = new Integracao();
            try {
                afn = integracao.montaListaNos(file.getAbsolutePath());
                menu_afd.setEnabled(true);
                JOptionPane.showMessageDialog(null, "O AFN foi importado. Já é possível efetuar a conversão para AFD!");
                
                for(int i=0; i<afn.size(); i++){
                    System.out.println("No: "+afn.get(i).getNome() + " do tipo "+ afn.get(i).getTipo() +" vai para:");
                    for(int e=0; e<afn.get(i).getArestas().size(); e++){
                        System.out.println(afn.get(i).getArestas().get(e).getNo().getNome() + " com valor: " + afn.get(i).getArestas().get(e).getValor());
                    }
                }
                
            } catch (IOException ex) {
                Logger.getLogger(InicioJF.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } else {
            System.out.println("File access cancelled by user.");
        }
        
    }//GEN-LAST:event_informarAfnImportarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InicioJF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InicioJF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InicioJF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioJF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioJF().setVisible(true);
                
            }
        });
        
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser escolherArquivo;
    private javax.swing.JMenuItem informarAfnImportar;
    private javax.swing.JMenuItem informarAfnManualmente;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JDesktopPane jdpPrincipal;
    private javax.swing.JMenuItem menu_afd;
    private javax.swing.JMenu menu_afn;
    private javax.swing.JMenu menu_ajuda;
    private javax.swing.JMenu menu_conversao;
    private javax.swing.JMenuItem menu_manual;
    private javax.swing.JMenu menu_sair;
    private javax.swing.JMenuItem menu_sobre;
    // End of variables declaration//GEN-END:variables
}
