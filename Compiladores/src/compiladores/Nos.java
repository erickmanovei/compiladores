/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores;

import java.util.ArrayList;

/**
 *
 * @author erick
 */
public class Nos {
    
    private String nome;
    private String tipo;
    private ArrayList<Arestas> arestas = new ArrayList<Arestas>();
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }   

    public ArrayList<Arestas> getArestas() {
        return arestas;
    }

    public void setArestas(ArrayList<Arestas> arestas) {
        this.arestas = arestas;
    }    

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }    
    public void addAresta(Arestas aresta){        
        arestas.add(aresta);
    }
}
